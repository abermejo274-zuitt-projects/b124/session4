package com.bermejo;

// inheritance
public class Adult extends Human {

    // properties
    private String occupation;

    // empty
    public Adult() {}

    // getter
    public String getOccupation() {
        return this.occupation;
    }

    // setter
    public void setOccupation(String newOccupation) {
        this.occupation = newOccupation;
    }

    // parameterized
    public Adult(String name, int age, char gender, String newOccupation) {
        super(name, age, gender);
        this.occupation = newOccupation;
    }

    // polymorphism
    // customized talk method
    public String talk() {
        return "Hello, I'm " + getName() + "! And I'm an adult.";
    }
}
