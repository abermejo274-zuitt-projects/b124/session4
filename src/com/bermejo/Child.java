package com.bermejo;

public class Child extends Human {

    // properties
    private String education;

    // empty
    public Child() {}

    // getter
    public String getEducation() {
        return this.education;
    }

    // setter
    public void setEducation(String newEducation) {
        this.education = newEducation;
    }

    // parameterized
    public Child(String name, int age, char gender, String newEducation) {
        super(name, age, gender);
        this.education = newEducation;
    }
}
