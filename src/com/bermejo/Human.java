package com.bermejo;

public class Human {

    // Mini-activity:
    // properties
    private String name;
    private int age;
    private char gender;

    // empty
    public Human() {}

    // parameterized
    public Human (String newName, int newAge, char newGender) {
        this.name = newName;
        this.age = newAge;
        this.gender = newGender;
    }

    // getter
    public String getName() {
        return this.name;
    }
    public int getAge() {
        return this.age;
    }
    public char getGender() {
        return this.gender;
    }

    // setter
    public void setName(String newName) {
        this.name = newName;
    }
    public void setAge(int newAge) {
        this.age = newAge;
    }
    public void setGender(char newGender) {
        this.gender = newGender;
    }

    // method
    public String talk() {
        return "Hello, my name is " + this.getName() + ".";
    }

}
