package com.bermejo;

public class Main {

    public static void main(String[] args) {

        // Mini-activity:
        Human a = new Human("Jose", 28, 'M');

        System.out.println(a.talk());

        // Test Inheritance
        Adult adult1 = new Adult("Juan", 35, 'M', "Lawyer");
        System.out.println(adult1.talk());
        System.out.println(adult1.getOccupation());

        // Mini activity
        Child child1 = new Child("Pedro", 26, 'M', "BSIT");
        System.out.println(child1.talk());
        System.out.println(child1.getEducation());
    }
}
